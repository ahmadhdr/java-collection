package programmer.zamannow.collection;

import java.util.Deque;
import java.util.LinkedList;

public class DequeApp {
    public static void main(String[] args) {
        Deque<String> stack = new LinkedList<>();
        stack.offer("ahmad");
        stack.offer("haidar");
        stack.offer("albaqir");

        for (String next = stack.pollLast(); next != null; next = stack.pollLast()) {
            System.out.println(next);
        }
    }
}
