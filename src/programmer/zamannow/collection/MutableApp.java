package programmer.zamannow.collection;

import programmer.zamannow.collection.data.Person;

import java.util.List;

public class MutableApp {
    public static void main(String[] args) {
        Person person = new Person("haidar");
        person.addHobby("Coding");
        person.addHobby("Reading a book");

        doSometingWithHobby(person.getHobbies());

        for(var hobby : person.getHobbies()) {
            System.out.println(hobby);
        }
    }
    // method ini merubah isi dari field hobbies, idealnya kita tidak boleh merubah properti secara langsung
    public static void doSometingWithHobby(List<String> hobbies) {
        hobbies.add("wkwk");
    }
}
