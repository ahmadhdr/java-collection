package programmer.zamannow.collection;

import java.util.HashMap;
import java.util.Map;

public class HashMapApp {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("name.first","ahmad");
        map.put("name.middle","haidar");
        map.put("name.last","albaqir");

        for(var name: map.keySet()) {
            System.out.println(map.get(name));
        }
    }
}
