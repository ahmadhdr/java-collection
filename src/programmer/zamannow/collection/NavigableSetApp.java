package programmer.zamannow.collection;

import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

public class NavigableSetApp {
    public static void main(String[] args) {
        NavigableSet<String> names = new TreeSet<String>();

        names.addAll(Set.of("ahmad","haidar","albaqir"));

        NavigableSet<String> reversedNames = names.descendingSet();

        for (var name: names) {
            System.out.println(name);
        }
    }
}
