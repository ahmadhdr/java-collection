package programmer.zamannow.collection;

import java.util.Arrays;
import java.util.List;

public class ArrayApp {
    public static void main(String[] args) {
        List<String> lists = List.of("ahmad","haidar","albaqir");

        Object[] objects = lists.toArray();
        String[] strings = lists.toArray(new String[]{});

        System.out.println(Arrays.toString(objects));
        System.out.println(Arrays.toString(strings));
    }
}
