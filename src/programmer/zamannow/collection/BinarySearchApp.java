package programmer.zamannow.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BinarySearchApp {
    public static void main(String[] args) {
        List<Integer> lists = new ArrayList<>(1000);
        for (int  i = 0; i < 1000; i++) {
            lists.add(i);
        }

        int index = Collections.binarySearch(lists,333);
        System.out.println(index);
    }
}
