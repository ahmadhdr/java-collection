package programmer.zamannow.collection;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class SetApp {
    public static void main(String[] args) {
        //Set<String> names = new HashSet<>();
        Set<String> names = new LinkedHashSet<>();
        names.add("hehe 1");
        names.add("hehe 2");
        names.add("hehe 3");
        names.add("hehe 1");

        for(var name: names) {
            System.out.println(name);
        }
    }
}
