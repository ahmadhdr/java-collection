package programmer.zamannow.collection;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class EntryApp {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
        map.put("ahmad","ahmad");
        map.put("haidar","haidar");
        map.put("albaqir","albaqir");

        Set<Map.Entry<String,String>> entry = map.entrySet();

        for (var value: entry) {
            System.out.println("key :" + value.getKey() + " - " + "value :" + value.getValue());
        }
    }
}
