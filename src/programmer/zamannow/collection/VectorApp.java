package programmer.zamannow.collection;

import java.util.List;
import java.util.Vector;

public class VectorApp {
    public static void main(String[] args) {
        List<String> list = new Vector<>();
        list.add("ahmad");
        list.add("haidar");
        list.add("albaqir");

        for (var value: list) {
            System.out.println(value);
        }
    }
}
