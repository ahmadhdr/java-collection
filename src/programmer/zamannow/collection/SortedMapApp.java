package programmer.zamannow.collection;

import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

public class SortedMapApp {



    public static void main(String[] args) {

        Comparator<String> stringComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        };

        SortedMap<String,String> map = new TreeMap<>(stringComparator);
        map.put("ahmad","ahmad");
        map.put("haidar","haidar");
        map.put("albaqir","albaqir");



        for (var value: map.keySet()) {
            System.out.println(value);
        }
    }
}
