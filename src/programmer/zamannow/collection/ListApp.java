package programmer.zamannow.collection;

import java.util.ArrayList;
import java.util.List;

public class ListApp {
    public static void main(String[] args) {
        List<String> names = new ArrayList(); // masukan data ketika ingin merubah default capacitinya

        names.add("ahmad");
        names.add("haidar");
        names.add("albaqir");
        names.set(0,"kevin");
        names.remove(2);

        for (var name: names) {
            System.out.println(name);
        }
    }
}
