package programmer.zamannow.collection;

import java.util.List;
import java.util.Spliterator;

public class SpliteratorApp {
    public static void main(String[] args) {
        List<String> lists = List.of("ahmad","haidar","albaqir","programmer","zaman","now");
        Spliterator<String> spliterator = lists.spliterator();
        Spliterator<String> spliterator1 = spliterator.trySplit();

        System.out.println(spliterator.estimateSize());
        System.out.println(spliterator1.estimateSize());
    }
}
