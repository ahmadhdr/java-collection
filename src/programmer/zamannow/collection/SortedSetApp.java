package programmer.zamannow.collection;

import programmer.zamannow.collection.data.Person;
import programmer.zamannow.collection.data.PersonComparator;

import java.util.SortedSet;
import java.util.TreeSet;


public class SortedSetApp  {
    public static void main(String[] args) {
        SortedSet<Person> persons = new TreeSet<>(new PersonComparator());

        persons.add(new Person("ahmad"));
        persons.add(new Person("haidar"));
        persons.add(new Person("albaqir"));

        for (var person: persons) {
            System.out.println(person.getName());
        }
    }
}
