package programmer.zamannow.collection;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class MapDefaultMethodApp {
    public static void main(String[] args) {
        Map<String,String> maps = new HashMap<>();
        maps.put("ahmad","ahmad");
        maps.put("haidar","haidar");
        maps.put("albaqir","albaqir");

        maps.forEach(new BiConsumer<String, String>() {
            @Override
            public void accept(String key, String value) {
                System.out.println("key :" + key + "value :" + value);
            }
        });
    }
}
