package programmer.zamannow.collection;

import java.util.Hashtable;
import java.util.Map;

public class HashTableApp {
    public static void main(String[] args) {
        Map<String,String> map = new Hashtable<>();
        map.put("ahmad","ahaidar");

        for (var value: map.keySet()) {
            System.out.println(value);
        }
    }
}
