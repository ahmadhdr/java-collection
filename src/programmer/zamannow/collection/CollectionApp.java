package programmer.zamannow.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CollectionApp {
    public static void main(String[] args) {
        Collection<String> names = new ArrayList<>();

        names.add("haidar");
        names.add("albaqir");
        names.addAll(Arrays.asList("Ahmad","haidar","albaqir"));

        for (var name: names) {
            System.out.println(name);
        }

        System.out.println("remove list");
        names.remove("albaqir");
        names.removeAll(List.of("haidar"));

        for (var name: names) {
            System.out.println(name);
        }
    }
}
