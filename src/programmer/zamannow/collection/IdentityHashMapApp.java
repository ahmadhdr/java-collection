package programmer.zamannow.collection;

import java.util.IdentityHashMap;
import java.util.Map;

public class IdentityHashMapApp {
    public static void main(String[] args) {
        String key1 = "name.first";

        String name = "name";
        String dot = ".";
        String first = "first";

        String key2 = name + dot + first;

        Map<String,String> map = new IdentityHashMap<>();
        map.put(key1, "Ahmad");
        map.put(key2, "haidar");

        System.out.println(key1.equals(key2)); // value equation
        System.out.println(key1 == key2); // refenrence equation
    }
}
