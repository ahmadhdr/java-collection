package programmer.zamannow.collection;

import java.util.Stack;

public class StackClassApp {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
        stack.push("haidar");
        stack.push("haidar");

        for (var value = stack.pop(); value != null; stack.pop()) {
            System.out.println(value);
        }
    }
}
