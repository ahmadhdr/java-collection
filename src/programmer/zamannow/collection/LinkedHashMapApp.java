package programmer.zamannow.collection;

import java.util.LinkedHashMap;
import java.util.Map;

public class LinkedHashMapApp {
    public static void main(String[] args) {
        Map<String,String> map  = new LinkedHashMap<>();

        map.put("first","Ahmad");
        map.put("middle","Haidar");
        map.put("last","Albaqir");

        for (var key: map.keySet()) {
            System.out.println(key);
            System.out.println(map.get(key));
        }

    }
}
