package programmer.zamannow.collection;

import java.util.EnumMap;
import java.util.Map;

public class EnumMapApp {
    public static enum Level {
        FREE, STANDARD, PREMIUM, VIP
    }

    public static void main(String[] args) {
        Map<Level,String> map = new EnumMap<Level, String>(Level.class);
        map.put(Level.FREE,"Eko 1");
        map.put(Level.VIP,"Eko 2");
        map.put(Level.PREMIUM,"Eko 3");
        map.put(Level.STANDARD,"Eko 4");

        for (var key: map.keySet()) {
            System.out.println(map.get(key));
        }
    }
}
