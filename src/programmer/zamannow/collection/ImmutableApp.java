package programmer.zamannow.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImmutableApp {
    public static void main(String[] args) {
        List<String> one = Collections.singletonList("Satu");
        List<String> emptu = Collections.emptyList();
        List<String> mutable = new ArrayList<>();
        List<String> immutable = Collections.unmodifiableList(mutable
        );

        List<String> elements = List.of("eko urniawok khannedy");
    }
}
