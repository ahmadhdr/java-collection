package programmer.zamannow.collection;

import java.util.NavigableMap;
import java.util.TreeMap;

public class NavigableMapApp {
    public static void main(String[] args) {
        NavigableMap<String,String> map = new TreeMap<>();

        map.put("ahmad","ahmad");
        map.put("haidar","haidar");
        map.put("albaqir","albaqir");

        System.out.println(map.lowerKey("haidar"));
        System.out.println(map.higherKey("albaqir"));
    }
}
