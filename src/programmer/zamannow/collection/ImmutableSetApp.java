package programmer.zamannow.collection;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ImmutableSetApp {
    public static void main(String[] args) {
        Set<String> empty = Collections.emptySet();
        Set<String> one = Collections.singleton("elp");

        Set<String> mutable = new HashSet<>();
        mutable.add("eko");
        mutable.add("kurniawan");
        Collections.unmodifiableCollection(mutable);
    }
}
