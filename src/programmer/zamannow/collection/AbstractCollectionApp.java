package programmer.zamannow.collection;

import programmer.zamannow.collection.collections.SingleQueue;

import java.util.Queue;

public class AbstractCollectionApp {
    public static void main(String[] args) {
        Queue<String> queue = new SingleQueue<>();
        queue.offer("ahmad");
        queue.offer("haidar");

        System.out.println(queue.size());

    }
}
